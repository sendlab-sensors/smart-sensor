//
// Created by Patrick on 28/05/2020.
//


#include <SmartSensor.h>
#include <board-support/util/PinManager.h>
#include "state/IdleState.h"

IdleState::IdleState(StateMachine* s_machine) : State(s_machine) {
}

void IdleState::init() {
    PinManager::digital_write(STATUS_LED_1_PIN, HIGH);
    _delay_ms(50);
    PinManager::digital_write(STATUS_LED_1_PIN, LOW);
}

void IdleState::update() {
    SerialLogger::debug("Went to idle state");
    _delay_ms(IDLE_INTERVAL);
    this->m_state_machine->set_state(S_POLL);
}

void IdleState::destroy() {

}
