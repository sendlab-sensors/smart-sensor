//
// Created by Patrick on 02/06/2020.
//

#include <board-support/util/PinManager.h>
#include <SmartSensor.h>
#include "state/PollState.h"

PollState::PollState(StateMachine *s_machine) : State(s_machine) {
}

void PollState::init() {
    PinManager::digital_write(STATUS_LED_2_PIN, HIGH);
    this->m_light_driver.begin();
}

void PollState::update() {
    SerialLogger::debug("Polling sensors");
    bool is_successful = m_ths_driver.update();
    if (is_successful) {
        SerialLogger::infof("[THS]: Temperature: %f, Humidity: %f\n",
                            this->m_ths_driver.get_temperature(),
                            this->m_ths_driver.get_humidity());
    } else {
        SerialLogger::error("[THS]: Failed to update, communication failure");
    }

    float als_lux;
    if (this->m_light_driver.get_als_lux(als_lux))
        SerialLogger::infof("[Light]: lux: %f\n", als_lux * 1500);

    _delay_ms(500);
#if USE_XBEE == 1
    this->m_state_machine->set_state(S_XBEE);
#else
    this->m_state_machine->set_state(S_IDLE);
#endif
}

void PollState::destroy() {
    PinManager::digital_write(STATUS_LED_2_PIN, LOW);
}
