//
// Created by Patrick on 02/06/2020.
//


#include <SmartSensor.h>
#include "state/XbeeConnState.h"

XbeeConnState::XbeeConnState(StateMachine *s_machine) : State(s_machine) {
}

void XbeeConnState::init() {
    SerialLogger::debug("- [XBEE] - initing XBEE");
}

void XbeeConnState::update() {
    SerialLogger::debug("- [XBEE] - In the XBEE state update loop");
    _delay_ms(5000);
    this->m_state_machine->set_state(S_IDLE);
}

void XbeeConnState::destroy() {

}
