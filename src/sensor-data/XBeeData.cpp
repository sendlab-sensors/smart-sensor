#include "sensor-data/XBeeData.h"

#include <stdio.h>
#include <string.h>

#define NAME_TEMPERATURE    "\"Temperature\""
#define NAME_HUMIDITY       "\"Humidity\""
#define NAME_ILLUMINANCE    "\"Illuminance\""

#define UNIT_TEMPERATURE    "\"C\""
#define UNIT_HUMIDITY       "\"%\""
#define UNIT_ILLUMINANCE    "\"lx\""

XBeeData::XBeeData(float temperature, float humidity, float illuminance) {
    this->m_temperature = temperature;
    this->m_humidity = humidity;
    this->m_illuminance = illuminance;

    initialize_packet();
}

void XBeeData::initialize_packet() {
    char buffer[PACKET_SIZE];
    char buffer_temperature[PACKET_SIZE];
    char buffer_humidity[PACKET_SIZE];
    char buffer_illuminance[PACKET_SIZE];

    snprintf(buffer_temperature, PACKET_SIZE, "%.2f", m_temperature);
    snprintf(buffer_humidity, PACKET_SIZE, "%.2f", m_humidity);
    snprintf(buffer_illuminance, PACKET_SIZE, "%.2f", m_illuminance);

    strcpy(buffer, "[{\"name\":");
    strcat(buffer, NAME_TEMPERATURE);
    strcat(buffer, ",\"value\":");
    strcat(buffer, buffer_temperature);
    strcat(buffer, ",\"unit\":");
    strcat(buffer, UNIT_TEMPERATURE);

    strcat(buffer, "},{\"name\":");
    strcat(buffer, NAME_HUMIDITY);
    strcat(buffer, ",\"value\":");
    strcat(buffer, buffer_humidity);
    strcat(buffer, ",\"unit\":");
    strcat(buffer, UNIT_HUMIDITY);

    strcat(buffer, "},{\"name\":");
    strcat(buffer, NAME_ILLUMINANCE);
    strcat(buffer, ",\"value\":");
    strcat(buffer, buffer_illuminance);
    strcat(buffer, ",\"unit\":");
    strcat(buffer, UNIT_ILLUMINANCE);

    strcat(buffer, "}]");

    strcpy(this->m_packet, buffer);
}

char *XBeeData::get_packet() {
    return this->m_packet;
}